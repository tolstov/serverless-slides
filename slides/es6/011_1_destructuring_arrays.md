### destructuring arrays

```js
const users = [
	{ 'id': 24, 'name': 'John Doe'},
	{ 'id': 25, 'name': 'Tom Neck'},
	{ 'id': 26, 'name': 'Mike Hoo'},   
];

const [john, tom, mike]  = users;
```

```js
const [john, ...others]  = users;
```
