### WeakSet
<small>The WeakSet object lets you store weakly held objects in a collection.</small>
```js
let tom = {name: 'Tom' };
var students = new WeakSet();
students.add(tom);
students.add({name:'Bob'});
students.add({name:'Li'});
console.log(students);
tom = null;

students.has({name: 'Tom'});
students.delete({name: 'Tom'});
```