### arrow functions

<small>An arrow function expression is a syntactically compact alternative to a regular function expression, although without its own bindings to the this, arguments, super, or new.target keywords. 
Arrow function expressions are ill suited as methods, and they cannot be used as constructors.</small>

```js
const names = ['Tom', 'Bob'];
const formattedNames= names.map(function (name) { 
	return name.toUpperCase();
});
console.log(formattedNames);
```

```js
const names = ['Tom', 'Bob'];
const formattedNames= names.map((name) => {
  return name.toUpperCase();
});
console.log(formattedNames);

//OR
const formattedNames= names.map(name => name.toUpperCase());
```
