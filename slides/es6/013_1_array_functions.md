### array functions

<small>find()</small>
```js
var ids = [5, 12, 8, 130, 44];

var found = ids.find(function(element) {
	return element > 10;
});

console.log(found);//12
```

<small>some()</small>
```js
var ids = [1, 2, 3, 4, 5];

var even = function(element) {
	return element % 2 === 0;
};

console.log(ids.some(even));//true
```

<small>every()</small>
```js
function isBelowThreshold(currentValue) {
	return currentValue < 40;
}

var ids = [1, 30, 39, 29, 10, 13];

console.log(ids.every(isBelowThreshold));//true
```

