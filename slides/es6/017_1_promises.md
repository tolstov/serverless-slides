### Promises

<small>Custom</small>
```js
const success = true;
const promise = new Promise((resolve, reject) => {
    
	setTimeout(() => {
		success ? resolve('Ok') : reject('Error');
	}, 1000);
});
promise.then((data) => console.log(data))
	.catch((err) => console.log(err));
```



