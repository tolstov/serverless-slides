### Generators
<small>Example</small>
```js
function ajax(url) {
  fetch(url).then(data => data.json()).then(() => makeRequests.next())
}

function * MakeRequests() {
  console.log('pokemon 1');
  yield ajax('https://pokeapi.co/api/v2/pokemon/1/');
  console.log('pokemon 2');
  yield ajax('https://pokeapi.co/api/v2/pokemon/2/');
  console.log('pokemon 3');
  yield ajax('https://pokeapi.co/api/v2/pokemon/3/');
}

  
const makeRequests = MakeRequests();
makeRequests.next();
```