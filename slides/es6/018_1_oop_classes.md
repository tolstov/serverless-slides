### OOP Classes

```js
class Person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
  say(message) {
   console.log(message);
  }
  
  get aboutMe() {
    console.log(`${this.name} ${this.age}`);
  }
  
  set setGender(value) {
    this.gender = value;
  }

  static info() {
    console.log('class Person');
  }
}
```

```js
const Person = class extends BasePerson {
  constructor(name, age) {
    super(name, age);
  }
}
```