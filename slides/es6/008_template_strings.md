### template literals

```js
//ES5
console.log('string text line 1\n' +
'string text line 2');
// "string text line 1
// string text line 2"
```


```js
//ES6
console.log(`string text line 1
string text line 2`);
// "string text line 1
// string text line 2"
```