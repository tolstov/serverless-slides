### Useful gems
```ruby
gem "interactor"
gem "rectify"
gem "trailblazer-operation"

gem "wisper"
gem 'rails-observers'

gem "draper"
```
