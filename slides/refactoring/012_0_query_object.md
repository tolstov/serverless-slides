### Query object​

Query Object encapsulate a single database query and any logic that it query needs to operate. It still uses ActiveRecord but adds some very light sugar on the top to make this style of architecture easier. This helps to keep your model classes lean and gives a natural home to this code.