### Solution

```ruby
# app/policies/offer_policy.rb
class OfferPolicy
  attr_reader :current_user, :resource

  def initialize(current_user, resource)
    @current_user = current_user
    @resource = resource
  end

  def able_to_create?
    resource.active? && current_user.company_id != resource.company_id
  end
end
```