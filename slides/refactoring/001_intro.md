# Refactoring in Rails

<img style="z-index: -1; opacity: 0.03;border: none; top: 0; left: 0; box-shadow: none; margin: 0; position: absolute" src="https://upload.wikimedia.org/wikipedia/commons/thumb/6/62/Ruby_On_Rails_Logo.svg/1200px-Ruby_On_Rails_Logo.svg.png"/>
<div align="right">
<br>
<p class="small author">
    <img class="img-circle" src="https://pbs.twimg.com/profile_images/1010525855506944001/WmojF1CV_400x400.jpg"/>
    <br>
    Ruslan Tolstov
    <br>
    <small>Ruby Developer at MLSDev</small>
</p>
</div>